# TDD Curry

TDD workshop implementing a higher order function to curry a function.

## Install Packages

`npm install`

## Running Tests

`npm run test`

`npm run test-watch`
