import _ from './_'
import curry from './curry'

describe('curry', () => {
  it('curries a single value', function () {
    var f = curry(function (a, b, c, d) {
      return (a + b * c) / d
    }) // f(12, 3, 6, 2) == 15
    var g = f(12)
    expect(g(3, 6, 2)).toBe(15)
  })
  it('curries multiple values', function () {
    var f = curry(function (a, b, c, d) {
      return (a + b * c) / d
    }) // f(12, 3, 6, 2) == 15
    var g = f(12, 3)
    expect(g(6, 2)).toBe(15)
    var h = f(12, 3, 6)
    expect(h(2)).toBe(15)
  })

  it('allows further currying of a curried function', function () {
    var f = curry(function (a, b, c, d) {
      return (a + b * c) / d
    }) // f(12, 3, 6, 2) == 15
    var g = f(12)
    expect(g(3, 6, 2)).toBe(15)
    var h = g(3)
    expect(h(6, 2)).toBe(15)
    expect(g(3, 6)(2)).toBe(15)
  })
  it('preserves context', function () {
    var valueToBindToThis = { x: 10 }
    var f = function (a, b) {
      return a + b * this.x
    }
    var g = curry(f)

    expect(g.call(valueToBindToThis, 2, 4)).toBe(42)
    expect(g.call(valueToBindToThis, 2).call(valueToBindToThis, 4)).toBe(42)
    expect(g(2).call(valueToBindToThis, 4)).toBe(42)
  })

  it('supports _ placeholder', function () {
    var f = function (a, b, c) {
      return [a, b, c]
    }
    var g = curry(f)

    const eq = (valueToTest, expectedValue) =>
      expect(valueToTest).toEqual(expectedValue)

    eq(g(1)(2)(3), [1, 2, 3])
    eq(g(1)(2, 3), [1, 2, 3])
    eq(g(1, 2)(3), [1, 2, 3])
    eq(g(1, 2, 3), [1, 2, 3])

    eq(g(_, 2, 3)(1), [1, 2, 3])
    eq(g(1, _, 3)(2), [1, 2, 3])
    eq(g(1, 2, _)(3), [1, 2, 3])

    eq(g(1, _, _)(2)(3), [1, 2, 3])
    eq(g(_, 2, _)(1)(3), [1, 2, 3])
    eq(g(_, _, 3)(1)(2), [1, 2, 3])

    eq(g(1, _, _)(2, 3), [1, 2, 3])
    eq(g(_, 2, _)(1, 3), [1, 2, 3])
    eq(g(_, _, 3)(1, 2), [1, 2, 3])

    eq(g(1, _, _)(_, 3)(2), [1, 2, 3])
    eq(g(_, 2, _)(_, 3)(1), [1, 2, 3])
    eq(g(_, _, 3)(_, 2)(1), [1, 2, 3])

    eq(g(_, _, _)(_, _)(_)(1, 2, 3), [1, 2, 3])
    eq(g(_, _, _)(1, _, _)(_, _)(2, _)(_)(3), [1, 2, 3])
  })
})
