import _ from './_'

export default function curry(functionToCurry) {
  const totalArguments = functionToCurry.length

  function curriedFunction(...currentArguments) {
    const placeholderCount = currentArguments.filter(
      (currentArgument) => currentArgument === _,
    ).length

    const isRemainingArguments =
      totalArguments - currentArguments.length + placeholderCount > 0

    const replacePlaceholders = (currentArguments, args) =>
      currentArguments.reduce(
        ([partiallyReplacedArguments, remainingArguments], currentArgument) =>
          currentArgument === _ && remainingArguments.length > 0
            ? [
                [...partiallyReplacedArguments, remainingArguments[0]],
                remainingArguments.slice(1),
              ]
            : [
                [...partiallyReplacedArguments, currentArgument],
                remainingArguments,
              ],
        [[], args],
      )

    const combineArguments = (
      partiallyReplacedArguments,
      remainingArguments,
    ) => [...partiallyReplacedArguments, ...remainingArguments]

    if (isRemainingArguments) {
      return function (...args) {
        return curriedFunction.call(
          this,
          ...combineArguments(...replacePlaceholders(currentArguments, args)),
        )
      }
    }
    return functionToCurry.apply(this, currentArguments)
  }
  return curriedFunction
}
